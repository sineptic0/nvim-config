local M = {}

M.general = {
    n = {
        [";"] = { ":" },

        ["<C-h>"] = { "<C-w>h", "Move cursor to left window" },
        ["<C-l>"] = { "<C-w>l", "Move cursor to right window" },
        ["<C-k>"] = { "<C-w>k", "Move cursor to top window" },
        ["<C-j>"] = { "<C-w>j", "Move cursor to bottom window" },

        ["<C-y>"] = { "<C-w>H", "Move window left" },
        ["<C-o>"] = { "<C-w>L", "Move window right" },
        ["<C-i>"] = { "<C-w>K", "Move window up" },
        ["<C-u>"] = { "<C-w>J", "Move window down" },
        ["<C-a>"] = { "<C-V>" },

        ["zz"] = { "za" },

        ["H"] = {
            function()
                vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
            end,
            "Toggle inlay hints",
        },

        -- TODO: Add same for `i` mode.
        ["<C-d>"] = { "<C-y>", "Scroll up" },
        ["<C-f>"] = { "<C-e>", "Scroll down" },
    },
    i = {
        ["<C-h>"] = { "<Left>", "Move left" },
        ["<C-l>"] = { "<Right>", "Move right" },
        ["<C-j>"] = { "<Down>", "Move down" },
        ["<C-k>"] = { "<Up>", "Move up" },

        ["jj"] = { "<Esc>", "Switch to normal mode" },
    },
    v = {
        -- ["J"] = { ":m '>+1<CR>gv=gv", "Move down" },
        -- ["K"] = { ":m '<-2<CR>gv=gv", "Move up" },
    },
}

M.telescope = {
    n = {
        ["<leader>ff"] = {
            function()
                require("telescope.builtin").find_files()
            end,
            "List files in current direcotory",
        },
        ["<leader>ss"] = {
            function()
                require("telescope.builtin").live_grep()
            end,
            "Search in current directory",
        },
        ["<leader>tb"] = {
            function()
                require("telescope.builtin").buffers()
            end,
            "Browse opened buffers",
        },
        ["<leader>to"] = {
            function()
                require("telescope.builtin").vim_options()
            end,
            "Vim options picker",
        },
        ["<leader>tc"] = {
            function()
                require("telescope.builtin").commands()
            end,
            "Commands picker",
        },

        ["<leader>ld"] = {
            function()
                require("telescope.builtin").lsp_document_symbols()
            end,
            "All names in file",
        },
        ["<leader>ls"] = {
            function()
                require("telescope.builtin").lsp_workspace_symbols()
            end,
            "All structs in project",
        },
    },
}

M.lazygit = {
    n = {
        ["<leader>lg"] = { "<cmd>LazyGit<cr>", "LazyGit" },
    },
}

M.lspconfig = {
    n = {
        ["K"] = { vim.lsp.buf.hover, "Lsp hover" },
        ["<leader>rn"] = { vim.lsp.buf.rename, "Rename under cursor" },
        ["F"] = { vim.lsp.buf.definition, "Definition" },
    },
}

M.comment = {}

M.actions_preview = {
    vn = {
        ["A"] = {
            function()
                require("actions-preview").code_actions()
            end,
            "Code actions picker",
        },
    },
}

M.conform = {
    n = {
        ["<leader>lf"] = {
            function()
                require("conform").format({
                    tymeout = 300,
                    async = true,
                    lsp_fallback = true,
                })
            end,
            "Format file",
        },
    },
}

M.lsp_lines = {
    n = {
        ["<leader>el"] = {
            require("core.virtual_diagnostic").toggle,
            "Toggle lsp lines",
        },
    },
}

M.trouble = {}

M.luasnip = {}
M.cmp = {}
M.whichkey = {}
M.catppuccin = {}
M.lualine = {}
M.mason = {}
M.treesitter = {}
M.noice = {}
M.indent_blank_line = {}
M.autopairs = {}
M.todo_comments = {}
M.devicons = {}
M.crates = {}
M.rustacean = {
    n = {
        ["<leader>ct"] = {
            "<cmd>RustTest<cr>",
            "Runs a test under the cursor",
        },
    },
}
M.ufo = {}
M.mason_lspconfig = {}
M.tiny_code_action = {
    vn = {
        ["A"] = {
            function()
                require("tiny-code-action").code_action()
            end,
            "Code actions picker",
        },
    },
}
M.notify = {}
M.nui = {}
M.lualine_lsp_progress = {}
M.outline = {}
M.render_markdown = {}
M.dap = {}
M.dap_ui = {}
M.surround = {}
M.go = {}
M.kanagawa = {}

return M
