local M = {}

local function to_chars(string)
    assert(type(string) == "string", "'string' argument must be string, but it's '" .. type(string) .. "'")

    local chars = {}
    for i = 1, #string do
        local c = string:sub(i, i)
        chars[i] = c
    end
    return chars
end

local function parse_mapping_properties(properties)
    assert(type(properties) == "table", "mappings 'properties' must be table, but it's '" .. type(properties) .. "'")
    local right_hand_side, description, opts
    for i, value in ipairs(properties) do
        if i == 1 then
            assert(
                type(value) == "string" or type(value) == "function",
                "'rhs' must be string or function, but it's '" .. type(value) .. "'"
            )
            right_hand_side = value
        elseif i == 2 then
            assert(type(value) == "string", "'description' must be string, but it's '" .. type(value) .. "'")
            description = value
        elseif i == 3 then
            assert(type(value) == "table", "'opts' must be table, but it's '" .. type(value) .. "'")
            opts = value
        else
            error("mapping options must be {'rhs', 'description', 'opts'}")
        end
    end
    assert(
        type(right_hand_side) == "string" or type(right_hand_side) == "function",
        "'rhs' must be string or function, but it's '" .. type(right_hand_side) .. "'"
    )
    return {
        right_hand_side = right_hand_side,
        description = description,
        opts = opts,
    }
end

M.get_mappings = function(section_name)
    if type(section_name) ~= "string" then
        error("section type must be 'string', but it's '" .. type(section_name) .. "'")
    end

    local A = {}

    local mappings_section = dofile("/home/sineptic/.config/nvim/lua/mappings/mappings.lua")[section_name]
    assert(type(mappings_section) == "table", "mappings config doesn't contain '" .. section_name .. "' section")
    for modes, mappings in pairs(mappings_section) do
        modes = to_chars(modes)
        for left_hand_side, properties in pairs(mappings) do
            assert(type(left_hand_side) == "string", "'lhs' must be string, but it's '" .. type(left_hand_side) .. "'")
            properties = parse_mapping_properties(properties)

            table.insert(A, {
                modes = modes,
                lhs = left_hand_side,
                rhs = properties.right_hand_side,
                description = properties.description,
                opts = properties.opts,
            })
        end
    end

    return A
end

local function register_mapping(modes, lhs, rhs, description, opts)
    opts.desc = description
    vim.keymap.set(modes, lhs, rhs, opts)

    -- local wk = require("which-key")
    -- if type(wk) ~= "nil" then
    --     for _, mode in ipairs(modes) do
    --         wk.register({
    --             [lhs] = { rhs, description },
    --         }, {
    --             mode = mode,
    --         })
    --     end
    -- end
end

M.load_mappings = function(section_name)
    local mappings = M.get_mappings(section_name)
    for _, mapping in ipairs(mappings) do
        register_mapping(mapping.modes, mapping.lhs, mapping.rhs, mapping.description or "", mapping.opts or {})
    end
end

return M
