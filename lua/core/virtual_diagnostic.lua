local M = {}

M.virtual_text_conf = {
    prefix = "",
}

M.virtual_lines_conf = {
    highlight_whole_line = false,
}

M.toggle_virtual_text = function()
    local conf = vim.diagnostic.config()
    local set_conf = vim.diagnostic.config
    if conf.virtual_text then
        set_conf({ virtual_text = false })
    else
        set_conf({ virtual_text = M.virtual_text_conf })
    end
end

M.toggle_virtual_lines = function()
    local conf = vim.diagnostic.config()
    local set_conf = vim.diagnostic.config
    if conf.virtual_lines then
        set_conf({ virtual_lines = false })
    else
        set_conf({ virtual_lines = M.virtual_lines_conf })
    end
end

M.toggle = function()
    M.toggle_virtual_text()
    M.toggle_virtual_lines()
end

return M
