local opt = vim.opt
local g = vim.g

-- replace <Tab> with spaces
opt.expandtab = true
-- show tabs as nth spaces
opt.tabstop = 4
-- number of spaces to use for indent
opt.shiftwidth = 4

opt.relativenumber = true
opt.number = true

g.mapleader = " "

-- vim.o.foldcolumn = "1" -- '0' is not bad
vim.o.foldcolumn = "auto:3"
vim.o.foldlevel = 99 -- Using ufo provider need a large value, feel free to decrease the value
vim.o.foldlevelstart = 99
vim.o.foldenable = true
vim.o.fillchars = [[fold: ,foldopen:-,foldsep:│,foldclose:+]]

local virtual_diagnostic = require("core.virtual_diagnostic")
vim.diagnostic.config({

    signs = false,

    virtual_text = virtual_diagnostic.virtual_text_conf,
    virtual_lines = false,

    -- update_in_insert = true,
    severity_sort = true,
})
