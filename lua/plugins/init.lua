local M = {}

local mappings = require("mappings")

local function plugin_properties(plug_conf)
    assert(type(plug_conf) == "table", "plugin config must be table")
    assert(type(plug_conf.name) == "string", "you must specify name in '" .. plug_conf[1] .. "' plugin")

    local keys = vim.tbl_map(function(mapping)
        return {
            [1] = mapping.lhs,
            [2] = mapping.rhs,
            mode = mapping.modes,
            desc = mapping.description,
        }
    end, require("mappings").get_mappings(plug_conf.name))
    plug_conf.keys = vim.tbl_extend("keep", keys, plug_conf.keys or {})
    -- local keys = {}
    -- for _, mapping in ipairs(plug_mappings) do
    --     table.insert(keys, {
    --         [1] = mapping.lhs,
    --         [2] = mapping.rhs,
    --         mode = mapping.modes,
    --     })
    -- end

    return {
        name = plug_conf.name,
        conf = plug_conf,
        manually_disabled = plug_conf.enabled == false,
    }
end

M.load_plugins = function()
    local names, configs = {}, {}
    for _, plugin_conf in ipairs(require("plugins.plugins")) do
        local temp = plugin_properties(plugin_conf)
        if not temp.manually_disabled then
            table.insert(names, temp.name)
            table.insert(configs, temp.conf)
        end
    end

    require("lazy").setup(configs)

    for _, name in ipairs(names) do
        -- mappings.load_mappings(name)
    end
end

return M
