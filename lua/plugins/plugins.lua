local plugins = {
    require("plugins.configs.lualine"),
    require("plugins.configs.lualine.lsp-progress"),

    require("plugins.configs.whichkey"),
    require("plugins.configs.telescope"),
    require("plugins.configs.lazygit"),
    require("plugins.configs.lspconfig"),
    require("plugins.configs.mason"),
    require("plugins.configs.treesitter"),
    require("plugins.configs.noice"),
    require("plugins.configs.indent_blank_line"),
    require("plugins.configs.cmp"),
    require("plugins.configs.luasnip"),
    require("plugins.configs.autopairs"),
    require("plugins.configs.todo_comments"),
    require("plugins.configs.comment"),
    require("plugins.configs.actions_preview"),
    require("plugins.configs.conform"),
    require("plugins.configs.lsp_lines"),
    require("plugins.configs.devicons"),
    require("plugins.configs.trouble"),
    require("plugins.configs.crates"),
    require("plugins.configs.rustacean"),
    require("plugins.configs.ufo"),
    require("plugins.configs.mason_lspconfig"),
    require("plugins.configs.tiny_code_action"),
    require("plugins.configs.notify"),
    require("plugins.configs.nui"),
    require("plugins.configs.outline"),
    require("plugins.configs.render_markdown"),
    require("plugins.configs.go"),

    -- require("plugins.configs.debug"),
    -- require("plugins.configs.debug.ui"),

    require("plugins.configs.surround"),
}

local colorshemes = require("plugins.configs.colorshemes")

table.move(colorshemes, 1, #colorshemes, #plugins + 1, plugins)

return plugins
