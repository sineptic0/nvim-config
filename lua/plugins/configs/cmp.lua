local function scroll_docs_down(fallback)
    if require("cmp").visible() then
        require("cmp").scroll_docs(4)
    else
        fallback()
    end
end
local function scroll_docs_up(fallback)
    if require("cmp").visible() then
        require("cmp").scroll_docs(-4)
    else
        fallback()
    end
end
local function confirm_completion(fallback)
    if require("cmp").visible() then
        if require("luasnip").expandable() then
            require("luasnip").expand()
        else
            require("cmp").confirm({ select = true, behavior = require("cmp").ConfirmBehavior.Insert })
        end
    else
        fallback()
    end
end
local function select_next_item(fallback)
    if require("cmp").visible() then
        require("cmp").select_next_item()
    elseif require("luasnip").locally_jumpable(1) then
        require("luasnip").jump(1)
    else
        fallback()
    end
end
local function select_prev_item(fallback)
    local cmp = require("cmp")
    local luasnip = require("luasnip")
    if cmp.visible() then
        cmp.select_prev_item()
    elseif luasnip.locally_jumpable(-1) then
        luasnip.jump(-1)
    else
        fallback()
    end
end
local function complete(fallback)
    -- if require("cmp").visible() then
    require("cmp").mapping.complete()
    -- else
    --     fallback()
    -- end
end

return {
    "hrsh7th/nvim-cmp",
    name = "cmp",
    event = "InsertEnter",
    dependencies = {
        "luasnip",
        "saadparwaiz1/cmp_luasnip",
        "hrsh7th/cmp-nvim-lsp",
    },
    opts = {
        completion = {
            keyword_length = 1,
        },
        sources = {
            {
                name = "nvim_lsp",
                entry_filter = function(entry)
                    return require("cmp.types").lsp.CompletionItemKind[entry:get_kind()] ~= "Text"
                end,
            },
            { name = "buffer" },
            { name = "luasnip" },
            { name = "crates" },
        },
        mapping = {
            ["<C-f>"] = function(fallback)
                scroll_docs_down(fallback)
            end,
            ["<C-d>"] = function(fallback)
                scroll_docs_up(fallback)
            end,

            ["<CR>"] = function(fallback)
                confirm_completion(fallback)
            end,

            ["<Tab>"] = function(fallback)
                select_next_item(fallback)
            end,
            ["<S-Tab>"] = function(fallback)
                select_prev_item(fallback)
            end,
        },
        window = {
            completion = {
                scrollbar = false,
                border = "none",
            },
            documentation = {
                max_width = 80,
                max_height = 40,
            },
        },
        snippet = {
            expand = function(args)
                require("luasnip").lsp_expand(args.body)
            end,
        },
        experimental = {
            ghost_text = true,
        },
    },
}
