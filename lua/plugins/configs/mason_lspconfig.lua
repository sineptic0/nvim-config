return {
    "williamboman/mason-lspconfig.nvim",
    name = "mason_lspconfig",
    dependencies = {
        "mason",
    },
    opts = {
        ensure_installed = {
            "lua_ls",
            "rust_analyzer",
            "ts_ls",
            "clangd",
            "tailwindcss",
            "pylsp",
        },
    },
}
