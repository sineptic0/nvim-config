return {
    "stevearc/conform.nvim",
    name = "conform",
    event = { "BufWritePre" },
    cmd = { "ConformInfo" },
    opts = {
        formatters_by_ft = {
            lua = { "stylua" },
            rust = { "rustfmt" },
            json = { "prettierd" },
            html = { "prettierd" },
            css = { "prettierd" },
            less = { "prettierd" },
            typescript = { "prettierd" },
            typescriptreact = { "prettierd" },
            go = {
                "goimports",
                "gofmt",
            },
            python = {
                "ruff",
            },
        },
        format_on_save = {
            timeout_ms = 500,
            lsp_fallback = true,
        },
    },
    init = function()
        vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
        require("conform").formatters.rustfmt = {
            options = {
                default_edition = "2021",
            },
        }
        require("conform").formatters.prettierd = {
            options = {
                tabWidth = 4,
            },
        }
    end,
}
