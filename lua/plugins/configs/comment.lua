return {
    'numToStr/Comment.nvim',
    name = "comment",
    lazy = false,
    opts = {
        padding = true,
        sticky = true,
        ignore = nil,
        toggler = { line = 'gcc', block = 'gbc' },
        opleader = { line = 'gc', block = 'gb' },
        extra = { above = 'gcO', below = 'gco', eol = 'gcA' },
        mappings = { basic = false, extra = false }, -- TODO: use api for mappings
        pre_hook = nil,
        post_hook = nil,
    },
}
