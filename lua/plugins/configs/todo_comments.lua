return {
    "folke/todo-comments.nvim",
    name = "todo_comments",
    event = "VeryLazy",
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = {
        signs = false,
        -- keywords recognized as todo comments
        keywords = {
            TODO = {
                icon = " ",
                color = "default",
            },
            FIX = {
                icon = " ",
                color = "error",
                alt = { "FIXME", "BUG", "FIXIT", "ISSUE" },
            },
            HACK = {
                icon = " ",
                color = "warning",
            },
            WARN = {
                icon = " ",
                color = "warning",
                alt = { "WARNING", "XXX" },
            },
            TEST = {
                icon = "󰤒 ",
                color = "warning",
                alt = { "TESTING", "PASSED", "FAILED" },
            },
            PERF = {
                icon = " ",
                color = "warning",
                alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" },
            },
            NOTE = {
                icon = " ",
                color = "hint",
                alt = { "INFO" },
            },
        },
        gui_style = {
            fg = "NONE", -- The gui style to use for the fg highlight group.
            bg = "BOLD", -- The gui style to use for the bg highlight group.
        },
        -- list of named colors where we try to extract the guifg from the
        -- list of highlight groups or use the hex color if hl not found as a fallback
        -- FIXME:
        colors = {
            error = { "DiagnosticError", "ErrorMsg", "#DC2626" },
            warning = { "DiagnosticWarn", "WarningMsg", "#FBBF24" },
            info = { "DiagnosticInfo", "#2563EB" },
            hint = { "DiagnosticHint", "#10B981" },
            default = { "Identifier", "#7C3AED" },

            -- test = { "Identifier", "#FF00FF" },

            -- error = { "TodoFgFIX" },
            -- warning = { "TodoFgWARN" },
            -- info = { "DiagnosticInfo", "#2563EB" },
            -- hint = { "DiagnosticHint", "#10B981" },
            -- default = { "Identifier", "#7C3AED" },
        },
        highlight = {
            keyword = "wide",
        },
        search = {
            command = "rg",
            args = {
                "--color=never",
                "--no-heading",
                "--with-filename",
                "--line-number",
                "--column",
            },
            -- regex that will be used to match keywords.
            -- don't replace the (KEYWORDS) placeholder
            pattern = [[\b(KEYWORDS):]], -- ripgrep regex
            -- pattern = [[\b(KEYWORDS)\b]], -- match without the extra colon. You'll likely get false positives
        },
    },
}
