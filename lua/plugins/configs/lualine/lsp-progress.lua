return {
    "arkav/lualine-lsp-progress",
    name = "lualine_lsp_progress",
    lazy = true,
    opts = {
        display_components = {
            "lsp_client_name",
            "spinner",
            {
                -- "title",
                -- "percentage",
                -- "message",
            },
        },
        timer = {
            progress_enddelay = 1000,
            spinner = 100,
            lsp_client_name_enddelay = 1000,
        },
        spinner_symbols = {
            "⡿",
            "⣟",
            "⣯",
            "⣷",
            "⣾",
            "⣽",
            "⣻",
            "⢿",
        },
    },
}
