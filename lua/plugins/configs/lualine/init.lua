return {
    "nvim-lualine/lualine.nvim",
    name = "lualine",
    lazy = false,
    dependencies = {
        "devicons",
        "lualine_lsp_progress",
    },
    opts = {
        options = {
            icons_enabled = false,
            component_separators = { left = "|", right = "|" },
            section_separators = { left = "", right = "" },
        },
        sections = {
            lualine_a = { "mode" },
            lualine_b = {
                "branch",
                -- "diff",
                "diagnostics",
            },
            lualine_c = {
                "filename", --[[ "lsp_progress" --]]
            },
            lualine_x = {},
            lualine_y = {},
            lualine_z = { "location" },
        },
    },
}
