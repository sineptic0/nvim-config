return {
    "nvim-treesitter/nvim-treesitter",
    name = "treesitter",
    build = ":TSUpdate",
    lazy = false,
    config = function()
        local configs = require("nvim-treesitter.configs")

        configs.setup({
            auto_install = true,
            ensure_installed = { "rust", "c", "lua", "vim", "vimdoc", "query", "go", "gomod", "gowork", "gosum" },
            highlight = { enable = true },
            indent = { enable = true },
        })
    end,
}
