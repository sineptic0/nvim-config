return {
    'windwp/nvim-autopairs',
    name = "autopairs",
    dependencies = { "cmp" },
    event = "InsertEnter",
    opts = {
        disable_in_macro = true,
        disable_in_visualblock = false,
        disable_in_replace_mode = true,
        enable_moveright = true,
        enable_afterquote = true,         -- add bracket pairs after quote
        enable_check_bracket_line = true, -- check bracket in same line
        enable_bracket_in_quote = true,
        enable_abbr = false,              -- trigger abbreviation
        break_undo = true,                -- switch for basic rule break undo sequence
        check_ts = false,
        map_cr = true,
        map_bs = true, -- map the <BS> key
    },
    init = function()
        -- Insert `(` after select function or method item
        local cmp_autopairs = require('nvim-autopairs.completion.cmp')
        local cmp = require('cmp')
        cmp.event:on(
            'confirm_done',
            cmp_autopairs.on_confirm_done()
        )
    end,
}
