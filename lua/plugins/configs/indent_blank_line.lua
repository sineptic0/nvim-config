return {
    "lukas-reineke/indent-blankline.nvim",
    name = "indent_blank_line",
    event = "VeryLazy",
    main = "ibl",
    opts = {
        indent = {
            char = "▏",
        },
        scope = {
            enabled = false,
        },
    },
}
