return {
    "ray-x/go.nvim",
    name = "go",
    enabled = false,
    dependencies = {
        "ray-x/guihua.lua",
        "lspconfig",
        "treesitter",
    },
    event = { "CmdlineEnter" },
    ft = { "go", "gomod" },
    build = ':lua require("go.install").update_all_sync()', -- if you need to install/update all binaries
    opts = true,
}
