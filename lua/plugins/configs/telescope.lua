return {
    "nvim-telescope/telescope.nvim",
    name = "telescope",
    tag = "0.1.7",
    cmd = "Telescope",
    dependencies = {
        "nvim-lua/plenary.nvim",
    },
    opts = {
        defaults = {
            default_mappings = {
                i = {
                    -- ["<C-/>"] = function(v) require("telescope.actions").which_key(v) end,
                    ["<C-/>"] = "which_key",
                    ["<C-_>"] = "which_key",

                    ["<C-k>"] = "move_selection_worse",
                    ["<C-j>"] = "move_selection_better",

                    ["<C-f>"] = "preview_scrolling_down",
                    ["<C-d>"] = "preview_scrolling_up",

                    ["<C-x>"] = "close",

                    ["<CR>"] = "select_default",
                },
                n = {
                    ["?"] = "which_key",

                    ["j"] = "move_selection_next",
                    ["k"] = "move_selection_previous",
                    ["gg"] = "move_to_top",
                    ["G"] = "move_to_bottom",

                    -- ["F"] = "open_qflist", TODO: what's this doing?

                    ["<C-f>"] = "preview_scrolling_down",
                    ["<C-d>"] = "preview_scrolling_up",

                    ["<esc>"] = "close",
                    ["q"] = "close",
                    ["x"] = "close",

                    ["<Space>"] = "toggle_selection",
                    ["<CR>"] = "select_default",
                },
            },
        },
    },
}
