return {
    "aznhe21/actions-preview.nvim",
    enabled = true,
    name = "actions_preview",
    dependencies = {
        "telescope",
        "lspconfig",
    },
    opts = {
        -- options for vim.diff(): https://neovim.io/doc/user/lua.html#vim.diff()
        diff = {
            ctxlen = 3,
            -- algorithm = "minimal",
            algorithm = "histogram",
            ignore_whitespace = true,
            ignore_whitespace_change = true,
            ignore_whitespace_change_at_eol = true,
            ignore_blank_lines = true,
        },
    },
}
