return {
    "rachartier/tiny-code-action.nvim",
    name = "tiny_code_action",
    enabled = false,
    dependencies = {
        "nvim-lua/plenary.nvim",
        "telescope",
    },
    event = "LspAttach",
    opts = {},
}
