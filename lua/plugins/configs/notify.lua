return {
    "rcarriga/nvim-notify",
    name = "notify",
    lazy = false,
    init = function()
        vim.notify = require("notify")
    end,
    opts = {
        stages = "static",
    },
}
