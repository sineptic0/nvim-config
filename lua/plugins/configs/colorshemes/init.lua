local kanagawa = require("plugins.configs.colorshemes.kanagawa")
local catppuccin = require("plugins.configs.colorshemes.catppuccin")

local theme = "kanagawa"

if theme == "kanagawa" then
    kanagawa.init = function()
        vim.cmd.colorscheme("kanagawa")
    end
elseif theme == "catppuccin" then
    catppuccin.init = function()
        vim.cmd.colorscheme("catppuccin-mocha")
    end
else
    error('"' .. theme .. '" theme not found')
end

return {
    kanagawa,
    catppuccin,
}
