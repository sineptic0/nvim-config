return {
    "mfussenegger/nvim-dap",
    name = "dap",
    dependencies = {
        "dap_ui",
    },
    init = function()
        local dap = require("dap")
        dap.adapters.gdb = {
            type = "executable",
            command = "lldb",
            name = "lldb",
        }

        dap.configurations.rust = {
            {
                name = "hello-world",
                type = "lldb",
                request = "launch",
                program = function()
                    return vim.fn.getcwd() .. "/target/debug/hello-world"
                end,
                cwd = "${workspaceFolder}",
                stopOnEntry = false,
            },
        }
    end,
}
