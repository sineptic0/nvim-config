return {
    "hedyhli/outline.nvim",
    name = "outline",
    lazy = false,
    opts = {
        outline_window = {
            position = "right",

            -- Whether width is relative to the total width of nvim
            -- When relative_width = true, this means take 25% of the total
            -- screen width for outline window.
            relative_width = true,
            width = 30,
        },
        preview_window = {
            auto_preview = true,
            width = 60,
            relative_width = true,
            border = "rounded",
            winblend = 10,
        },
        keymaps = {
            fold_all = {},
            unfold_all = {},
            fold = {},
            unfold = {},
            fold_reset = {},
            code_actions = {},
            rename_symbol = {},
            restore_location = {},

            goto_location = "h",
            goto_and_close = "<Cr>",

            fold_toggle = "<Leader>",
            fold_toggle_all = "Z",
        },
    },
}
