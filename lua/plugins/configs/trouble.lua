return {
    "folke/trouble.nvim",
    name = "trouble",
    dependencies = { "devicons" },
    lazy = false,
    keys = {
        {
            "<leader>xx",
            "<cmd>Trouble diagnostics toggle<cr>",
            desc = "Diagnostics (Trouble)",
        },
        {
            "<leader>xX",
            "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
            desc = "Buffer Diagnostics (Trouble)",
        },
        {
            "<leader>la",
            "<cmd>Trouble lsp toggle<cr>",
            desc = "LSP Definitions / references / ... (Trouble)",
        },
        {
            "<leader>td",
            "<cmd>Trouble todo toggle win={position='down', size=0.3} preview={type='main'}<cr>",
            desc = "all TODO's",
        },
    },
    opts = {
        focus = true,
        auto_close = true,
        win = {
            position = "right",
            size = 0.5,
        },
        preview = {
            type = "split",
            size = 0.35,
            relative = "win",
        },
    },
}
