return {
    "neovim/nvim-lspconfig",
    name = "lspconfig",
    lazy = false,
    dependencies = {
        "cmp",
        "mason",
        "mason_lspconfig",
    },
    init_options = {
        userLanguages = {
            eelixir = "html-eex",
            eruby = "erb",
            rust = "html",
        },
    },
    config = function()
        -- of json
        local lspconf = require("lspconfig")
        local capabilities = require("cmp_nvim_lsp").default_capabilities()
        lspconf.lua_ls.setup({
            capabilities = capabilities,
        })
        lspconf.html.setup({
            capabilities = capabilities,
        })
        lspconf.clangd.setup({
            capabilities = capabilities,
        })
        lspconf.cssls.setup({
            capabilities = capabilities,
            init_options = {
                provideFormatter = false,
            },
        })
        lspconf.ts_ls.setup({
            capabilities = capabilities,
        })
        lspconf.tailwindcss.setup({
            capabilities = capabilities,
        })
        lspconf.typos_lsp.setup({
            capabilities = capabilities,
            cmd_env = { RUST_LOG = "error" },
            init_options = {
                config = "~/dotfiles/typos.toml",
                diagnosticSeverity = "Warning",
            },
        })
        lspconf.gopls.setup({
            capabilities = capabilities,
        })
        lspconf.pylsp.setup({
            capabilities = capabilities,
        })
    end,
}
