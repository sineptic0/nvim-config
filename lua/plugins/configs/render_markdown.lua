return {
    "MeanderingProgrammer/render-markdown.nvim",
    name = "render_markdown",
    enabled = false, -- FIXME: disable for hover
    lazy = false,
    dependencies = { "treesitter", "devicons" },
    opts = {
        heading = {
            enabled = true,
            sign = true,
            position = "overlay",
            --  inline: underlying '#'s are concealed resulting in a left aligned icon
            --  overlay: result is left padded with spaces to hide any additional '#'
            icons = {},
            width = "block",
            --  block: width of the heading text
            --  full: full width of the window

            -- backgrounds = {
            --     "RenderMarkdownH1Bg",
            --     "RenderMarkdownH2Bg",
            --     "RenderMarkdownH3Bg",
            --     "RenderMarkdownH4Bg",
            --     "RenderMarkdownH5Bg",
            --     "RenderMarkdownH6Bg",
            -- },
            backgrounds = {
                "",
            },
            foregrounds = {
                "RenderMarkdownH1",
                "RenderMarkdownH2",
                "RenderMarkdownH3",
                "RenderMarkdownH4",
                "RenderMarkdownH5",
                "RenderMarkdownH6",
            },
        },
        code = {
            enabled = true,
            sign = true,
            style = "full",

            position = "left",
            disable_background = { "diff" },
            -- An array of language names for which background highlighting will be disabled
            -- Likely because that language has background highlights itself

            width = "block",
            left_pad = 0,
            right_pad = 3,
            border = "thin",
            above = "▄",
            below = "▀",

            highlight = "RenderMarkdownCode",
            highlight_inline = "RenderMarkdownCodeInline",
        },
        bullet = {
            enabled = true,

            icons = { "-" },
            left_pad = 1,
            right_pad = 0,

            highlight = "RenderMarkdownBullet",
        },
        -- Checkboxes are a special instance of a 'list_item' that start with a 'shortcut_link'
        -- There are two special states for unchecked & checked defined in the markdown grammar
        checkbox = {
            enabled = true,
            unchecked = {
                icon = " 󰄱",
                highlight = "RenderMarkdownUnchecked",
            },
            checked = {
                icon = " 󰱒",
                highlight = "RenderMarkdownChecked",
            },
            -- TODO: add custom https://github.com/MeanderingProgrammer/render-markdown.nvim?tab=readme-ov-file#checkboxes
            custom = {
                todo = { raw = "[-]", rendered = "󰥔", highlight = "RenderMarkdownTodo" },
            },
        },
        quote = {
            enabled = true,

            icon = "▋",
            repeat_linebreak = false,

            highlight = "RenderMarkdownQuote",
        },
        -- TODO: configure tables https://github.com/MeanderingProgrammer/render-markdown.nvim?tab=readme-ov-file#tables
        -- TODO: configure callouts https://github.com/MeanderingProgrammer/render-markdown.nvim?tab=readme-ov-file#callouts
        link = {
            enabled = true,

            image = "󰥶",
            hyperlink = "󰌹",
            highlight = "RenderMarkdownLink",
            custom = {
                web = { pattern = "^http[s]?://", icon = "", highlight = "RenderMarkdownLink" },
            },
            -- TODO: add custom
        },
    },
}
